package com.htc.ea.departmentservice.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ResponseHttpExceptionHandler extends ResponseEntityExceptionHandler{
	

	private Logger log = LoggerFactory.getLogger(getClass());
	private static final String MSG_ERROR = "Error";

	/*
	 * Maneja todas las exception que no han sido controladas
	 * retorna la descripcion del error en el header
	 */
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		log.error("Exception: "+ex.toString());
		HttpHeaders headers = new HttpHeaders();
		headers.set(MSG_ERROR, ex.toString());
		return new ResponseEntity<>(null,headers,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/*
	 * Obtiene todos los errores de entrada del usuario
	 * y los retorna en el header bajo la etiqueta: Error
	 * (non-Javadoc)
	 * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleMethodArgumentNotValid(org.springframework.web.bind.MethodArgumentNotValidException, org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus, org.springframework.web.context.request.WebRequest)
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
		HttpHeaders headers, HttpStatus status, WebRequest request) {
		for (Object error : ex.getBindingResult().getAllErrors()) {
			if(error instanceof FieldError) {
		        FieldError fieldError = (FieldError) error;
		        String field = fieldError.getField();
		        if(field.contains(".")) field = field.split("\\.")[1];
		        StringBuilder builder = new StringBuilder();
		        builder.append("variable: ");
		        builder.append(field);
		        builder.append(" ");
		        builder.append(fieldError.getDefaultMessage());
		        headers.add(MSG_ERROR, builder.toString());   
			}
		}
		return new ResponseEntity<>(null,headers, HttpStatus.BAD_REQUEST);
	}	
}
