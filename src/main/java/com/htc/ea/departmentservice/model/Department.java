package com.htc.ea.departmentservice.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity(name="department")
public class Department implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1503362616213898666L;
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)//autoincrementable
	@Id
	@Column(name="id")
	private Long id;
	@Column(name="id_organization")
	private Long idOrganization;
	@Column(name="name")
	private String name;
	
	@Transient
	private List<Employee> employees = new ArrayList<>();

	public Department() {
		
	}

	public Department(Long idOrganization, String name) {
		super();
		this.idOrganization = idOrganization;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getIdOrganization() {
		return idOrganization;
	}

	public void setIdOrganization(Long idOrganization) {
		this.idOrganization = idOrganization;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Department [id=");
		builder.append(id);
		builder.append(", idOrganization=");
		builder.append(idOrganization);
		builder.append(", name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}

	

}
