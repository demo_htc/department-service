package com.htc.ea.departmentservice.config;


import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {
	/**
	 * configuracion para la documentacion de la API
	 * @return
	 */
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)
          .apiInfo(apiInfo())
          .select()                                  
          .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
          .paths(PathSelectors.any())       
          .build();                                           
    }
	
	/**
	 * Informacion que se va a desplegar sobre la API
	 * @return
	 */
	private ApiInfo apiInfo() {
	    return new ApiInfo(
	      "Department-Service", 
	      "Servicio de control de departamentos de una organizacion.", 
	      "v1.0", 
	      "Terms of service", 
	      new Contact("HTC", "http://www.hightech-corp.com/", ""), 
	      "License of API", "API license URL", Collections.emptyList());
	}
}
