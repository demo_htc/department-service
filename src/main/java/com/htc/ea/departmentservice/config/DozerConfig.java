package com.htc.ea.departmentservice.config;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DozerConfig {
	/**
	 * configuracion para hacer el mapeo de los DTO
	 * @return Mapper
	 */
	@Bean
	public Mapper beanMapper() {
		return new DozerBeanMapper();
	}
}
