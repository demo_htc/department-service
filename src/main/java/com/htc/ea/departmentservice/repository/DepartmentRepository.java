package com.htc.ea.departmentservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.htc.ea.departmentservice.model.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long>{
	/**
	 * Busca todos los departamentos bajo el id de la organization dado
	 * esta funcion se implementa internamente en JPA
	 * @param idOrganization
	 * @return lista de departamentos encontrados
	 */
	public List<Department> findAllByIdOrganization(Long idOrganization);
}
