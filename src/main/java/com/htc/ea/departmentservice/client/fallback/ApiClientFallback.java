package com.htc.ea.departmentservice.client.fallback;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.htc.ea.departmentservice.client.ApiClient;
import com.htc.ea.departmentservice.model.Employee;
import com.htc.ea.departmentservice.util.AppConst;

import feign.hystrix.FallbackFactory;

@Component
public class ApiClientFallback implements  FallbackFactory<ApiClient> {

	private Logger log = LoggerFactory.getLogger(getClass());	

	/**
	 * Captura el error en la llamada al servicio
	 */
	@Override
	public ApiClient create(Throwable cause) {
		
		log.error("Feign error: {}",cause);
		
		return new ApiClient() {
			
			@Override
			public ResponseEntity<Boolean> organizationExist(Long id) {
				log.error("Entrada a metodo de respaldo: organizationExist");
				HttpHeaders headers = new HttpHeaders();
				headers.add(AppConst.MSG_ERROR, "No se pudo conectar al servicio: Organization");
				return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
			}
			
			@Override
			public ResponseEntity<List<Employee>> findEmployeesByIdDepartment(Long id) {
				log.error("Entrada a metodo de respaldo: findEmployeesByIdDepartment");
				HttpHeaders headers = new HttpHeaders();
				headers.add(AppConst.MSG_ERROR, "No se pudo conectar al servicio: Employee");
				return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
			}
		};
	}
}
