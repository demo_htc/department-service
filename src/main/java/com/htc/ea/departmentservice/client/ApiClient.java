package com.htc.ea.departmentservice.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.htc.ea.departmentservice.client.fallback.ApiClientFallback;
import com.htc.ea.departmentservice.model.Employee;

/**
 * Interfaz de consumo de APIs mediante el Gateway de Spring
 * Posee implementacion de la interfaz la cual captura los errores de conexion
 * @author htc
 *
 */
@FeignClient(name = "spring-gateway-service" , fallbackFactory = ApiClientFallback.class)
public interface ApiClient {
	
	/**
	 * consulta al microservicio employee
	 * retorna lista de empleados que esten bajo el departamento enviado
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id departamento
	 * @return response entity con headers y lista de employee en el body
	 */
	@GetMapping(value="/employees/departments/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<Employee>> findEmployeesByIdDepartment(@PathVariable("id") Long id);
	
	/**
	 * consulta a microservicio organization
	 * retorna un valor boolean en el body donde: es true si se encontro, false si no
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id organization
	 * @return response entity con headers y valor boolean en el body
	 */
	@GetMapping(value="/organizations/{id}/exist",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> organizationExist(@PathVariable("id") Long id);

}
